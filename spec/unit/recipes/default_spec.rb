# Cookbook Name:: omnibus-gitlab
# Recipe:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'
require 'chef-vault'

describe 'omnibus-gitlab::default' do
  context 'with basic gitlab.rb' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
        node.normal['omnibus-gitlab']['gitlab_rb']['external_url'] = 'http://herpderp-extern.com'
        node.normal['omnibus-gitlab']['gitlab_rb']['pages_external_url'] = 'http://herpderp-pages.com'
        node.normal['omnibus-gitlab']['gitlab_rb']['logging']['udp_log_shipping_host'] = '127.0.0.1'
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.not_to raise_error
    end
    it 'creates gitlab.rb' do
      expect(chef_run).to create_template('/etc/gitlab/gitlab.rb').with(
        mode: '0600'
      )
    end
    it 'sets the logging name' do
      expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
        expect(c).to include("external_url 'http://herpderp-extern.com'")
        expect(c).to include("pages_external_url 'http://herpderp-pages.com'")
        expect(c).to include("logging['udp_log_shipping_hostname'] = 'fauxhai.local'")
      }
    end
  end
end
